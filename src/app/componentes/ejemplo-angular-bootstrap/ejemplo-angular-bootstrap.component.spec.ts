import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploAngularBootstrapComponent } from './ejemplo-angular-bootstrap.component';

describe('EjemploAngularBootstrapComponent', () => {
  let component: EjemploAngularBootstrapComponent;
  let fixture: ComponentFixture<EjemploAngularBootstrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploAngularBootstrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploAngularBootstrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
